const connect = require("../db/connect");

module.exports = class UsuarioController {
  static async postLogin(req, res) {
    const { email, senha } = req.body;

    if (!email || !senha) {
      return res.status(400).json({ error: "Email e senha são obrigatórios" });
    }

    const query = `SELECT * FROM usuario WHERE email = ? AND senha = ?`;

    try {
      connect.query(query, [email, senha], function (err, results) {
        if (err) {
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        if (results.length === 0) {
          return res.status(401).json({ error: "Credenciais inválidas" });
        }

        return res.status(200).json({ message: "Login realizado com sucesso", user: results[0] });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  static async createUsuario(req, res) {
    const { nome, email, senha } = req.body;

    if (!nome || !email || !senha) {
      return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
    } else if (!email.includes("@")) {
      return res.status(400).json({ error: "Email inválido. Deve conter @" });
    } else {
      const query = `INSERT INTO usuario (nome, email, senha) VALUES (?, ?, ?)`;
      const values = [nome, email, senha];

      try {
        connect.query(query, values, function (err) {
          if (err) {
            if (err.code === 'ER_DUP_ENTRY') {
              return res.status(400).json({ error: "Email já cadastrado" });
            } else {
              console.error(err);
              return res.status(500).json({ error: "Erro interno do servidor" });
            }
          }
          return res.status(201).json({ message: "Usuário criado com sucesso" });
        });
      } catch (error) {
        console.error("Erro ao executar a consulta:", error);
        res.status(500).json({ error: "Erro interno do servidor" });
      }
    }
  }

  static async getAllUsuarios(req, res) {
    const query = `SELECT * FROM usuario`;

    try {
      const results = await new Promise((resolve, reject) => {
        connect.query(query, function (err, results) {
          if (err) {
            return reject(err);
          }
          resolve(results);
        });
      });

      if (!results || results.length === 0) {
        return res.status(404).json({ message: "Nenhum usuário encontrado" });
      }

      return res.status(200).json({ message: "Obtendo todos os usuários", usuarios: results });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  static async getUsuarioById(req, res) {
    const { id_aluno } = req.params;
    const query = `SELECT * FROM usuario WHERE id_aluno = ?`;

    connect.query(query, [id_aluno], (err, result) => {
      if (err) {
        console.log("Erro: " + err);
        return res.status(500).json({ error: "Erro ao buscar usuário" });
      }
      if (result.length === 0) {
        return res.status(404).json({ message: "Usuário não encontrado" });
      }
      res.status(200).json({ message: "Usuário encontrado", usuario: result[0] });
    });
  }

  static async updateUser(req, res) {
    const { id_aluno } = req.params;
    const { nome, email, senha } = req.body;

    if (!nome || !email || !senha) {
      return res.status(400).json({ error: "Todos os campos são obrigatórios" });
    }

    if (nome.length > 255) {
      return res.status(400).json({ error: "O nome deve ter no máximo 255 caracteres" });
    }

    const emailValid = /^[a-zA-Z0-9._%+-]+@(?:gmail|outlook|yahoo)\.(?:com|com\.br|net|org)$/i;
    if (!emailValid.test(email)) {
      return res.status(400).json({ error: "Formato de email inválido ou provedor não suportado" });
    }

    const query = `UPDATE usuario SET nome = ?, email = ?, senha = ? WHERE id_aluno = ?`;
    const values = [nome, email, senha, id_aluno];

    connect.query(query, values, (err, result) => {
      if (err) {
        console.log("Erro: " + err);
        return res.status(500).json({ error: "Erro ao atualizar usuário" });
      }
      if (result.affectedRows === 0) {
        return res.status(404).json({ message: "Usuário não encontrado" });
      }
      res.status(200).json({ message: "Usuário atualizado com sucesso" });
    });
  }

  static async deleteUser(req, res) {
    const { id_aluno } = req.params;
    const query = `DELETE FROM usuario WHERE id_aluno = ?`;

    connect.query(query, [id_aluno], (err, result) => {
      if (err) {
        console.log("Erro: " + err);
        return res.status(500).json({ error: "Erro ao excluir usuário" });
      }
      if (result.affectedRows === 0) {
        return res.status(404).json({ message: "Usuário não encontrado" });
      }
      res.status(200).json({ message: "Usuário excluído com sucesso" });
    });
  }
};
