const connect = require("../db/connect");

module.exports = class ReservaController {
  static async createSchedule(req, res) {
    // Extrair os campos do corpo da solicitação
    const { fk_id_gestão, fk_id_docente, dateStart, dateEnd, timeStart, timeEnd, days, user, classroom, finalidade, status } = req.body;

    // Verificar se todos os campos necessários foram fornecidos
    if (!fk_id_gestão || !fk_id_docente || !dateStart || !dateEnd || !timeStart || !timeEnd || !days || !user || !classroom || !finalidade || !status) {
      return res.status(400).json({ error: "Preencha todos os campos" });
    }

    try {
      // Consulta SQL para inserir uma nova reserva
      const insertQuery = `INSERT INTO reserva (fk_id_gestão, fk_id_docente, dateStart, dateEnd, timeStart, timeEnd, days, user, classroom, finalidade, status)
                           VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;

      // Valores a serem inseridos na consulta
      const insertValues = [fk_id_gestão, fk_id_docente, dateStart, dateEnd, timeStart, timeEnd, days, user, classroom, finalidade, status];

      // Executar a consulta de inserção
      connect.query(insertQuery, insertValues, function (err) {
        if (err) {
          console.error(err);
          return res.status(500).json({ error: "Erro ao cadastrar agendamento" });
        }
        return res.status(201).json({ message: "Agendamento cadastrado com sucesso" });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno no servidor" });
    }
  }

  // Função para buscar todas as reservas
  static async getAllSchedules(req, res) {
    try {
      // Consulta SQL para buscar todas as reservas
      const query = 'SELECT * FROM reserva';

      // Executar a consulta de busca
      connect.query(query, function (err, results) {
        if (err) {
          return res.status(500).json({ error: "Erro ao buscar agendamentos" });
        }
        return res.status(200).json(results);
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno no servidor" });
    }
  }

  // Função para buscar uma reserva por ID
  static async getScheduleById(req, res) {
    const { id } = req.params;
    try {
      // Consulta SQL para buscar uma reserva por ID
      const query = `SELECT * FROM reserva WHERE id = ?`;

      // Executar a consulta de busca com o ID fornecido
      connect.query(query, [id], function (err, results) {
        if (err) {
          return res.status(500).json({ error: "Erro ao buscar agendamento" });
        }
        if (results.length > 0) {
          return res.status(200).json(results[0]);
        } else {
          return res.status(404).json({ error: "Agendamento não encontrado" });
        }
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno no servidor" });
    }
  }

  // Função para atualizar uma reserva
  static async updateSchedule(req, res) {
    const { id } = req.params;
    const { fk_id_gestão, fk_id_docente, dateStart, dateEnd, timeStart, timeEnd, days, user, classroom, finalidade, status } = req.body;

    // Verificar se todos os campos necessários foram fornecidos
    if (!fk_id_gestão || !fk_id_docente || !dateStart || !dateEnd || !timeStart || !timeEnd || !days || !user || !classroom || !finalidade || !status) {
      return res.status(400).json({ error: "Preencha todos os campos" });
    }

    try {
      // Consulta SQL para atualizar uma reserva
      const updateQuery = `UPDATE reserva SET
                           fk_id_gestão = ?,
                           fk_id_docente = ?,
                           dateStart = ?,
                           dateEnd = ?,
                           timeStart = ?,
                           timeEnd = ?,
                           days = ?,
                           user = ?,
                           classroom = ?,
                           finalidade = ?,
                           status = ?
                           WHERE id = ?`;

      // Valores a serem atualizados na consulta
      const updateValues = [fk_id_gestão, fk_id_docente, dateStart, dateEnd, timeStart, timeEnd, days, user, classroom, finalidade, status, id];

      // Executar a consulta de atualização
      connect.query(updateQuery, updateValues, function (err) {
        if (err) {
          console.error(err);
          return res.status(500).json({ error: "Erro ao atualizar agendamento" });
        }
        return res.status(200).json({ message: "Agendamento atualizado com sucesso" });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno no servidor" });
    }
  }

  // Função para excluir uma reserva
  static async deleteSchedule(req, res) {
    const { id } = req.params;

    try {
      // Consulta SQL para excluir uma reserva
      const deleteQuery = `DELETE FROM reserva WHERE id = ?`;

      // Executar a consulta de exclusão com o ID fornecido
      connect.query(deleteQuery, [id], function (err) {
        if (err) {
          console.error(err);
          return res.status(500).json({ error: "Erro ao deletar agendamento" });
        }
        return res.status(200).json({ message: "Agendamento deletado com sucesso" });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno no servidor" });  
    }
  }
};
